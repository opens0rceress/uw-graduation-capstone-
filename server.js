/*
*	Database API for the Virtual Academic Advisor
*	Author: Christine Sutton
*	University of Washington Capstone Project - Summer 2018
*
*	This API provides access to all possible database queries for the web based application at virtual-advisor.com. Data is returned in JSON format. 
*	For server installation details and necessary modules, please refer to the accompanying capstone documentation. 
*/
var express = require("express");
var bodyParser = require("body-parser");
var app = express(); 
var sql = require("mssql");

// Body Parser Middleware
app.use(bodyParser.json()); 

//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,PATCH");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//Setting up server
 var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
 });

var dbConfig = {
	//Database credentials have been redacted for gitlab display
    user: "",
    password: "",
    server: "65.175.68.xx",
    database: "",  
};

var  executeQuery = function(res, query){                          
     sql.connect(dbConfig, function (err) {
         if (err) {   
                     console.log("Error while connecting database :- " + err);
                     res.send(err);
                  }
					else {
                         // create Request object
                         var request = new sql.Request();
                         // query to the database
                        request.query(query, function (err, rs) {
						if (err) {
							console.log("Error while querying database : " + err);
							res.send(err);
						}
						else {
							res.send(rs);
						}
				  });     
			}		
	}); 
};    


/* API Section 
/ *******READ FIRST*******
/ Each API is classified by role: Advisor, Student, Administrator, or combination of roles.
/ Combined roles indicates identical functionality for that process across multiple roles. This will save you some time if you're the next programmer to take this project!
/ Locations of use for each API are included in their notes	
*/

/* Advisor: Dashboard */
//Get first name, last name, and student id for each student the advisor is currently advising
//Assumption: the Advisor's id in ACCOUNTS is used for reference 
app.get("/api/advisordashboard/:id", function(req , res){
	sql.close();
	var query = "SELECT first_name, last_name, StudentID FROM [ACCOUNTS], [student], [STUDENTS] WHERE ACCOUNTS.id = STUDENTS.accounts_id AND STUDENTS.student_id = student.StudentID AND student.AdvisorId = " + req.params.id;  //Use body or params here?
	executeQuery(res, query);
});

/* Advisor/Student: Get Student Preferences */
//gets all of the student data and plan preferences from their respective tables
//Assumption: StudentID in table student is used for reference (stored in the front end session storage) 
app.get("/api/studentinfo/:id", function(req , res){
	sql.close();
	var query = "SELECT first_name, last_name, email, MajorID, DepartmentID, JobTypeID, Status, TransferID from [ACCOUNTS], [STUDENT], [Students] WHERE students.accounts_id = ACCOUNTS.id AND students.student_id=" + req.params.id;
	executeQuery(res, query);
});

/* Advisor/Student: Edit Student Preferences */
//Updates the student preferences in their respective tables. 
//Assumption: Based off updating ACCOUNTS and student separately, so both IDs are needed to be passed from the front end
//Assumption: Updates all preferences. UI doesn't have to track which ones were changed, just send the updated data with the old data.  
app.put("/api/editaccountprefs", function(req , res){
	sql.close();
	var query = "UPDATE [ACCOUNTS] SET first_name=" + req.body.first_name + ", last_name=" + req.body.last_name + ", email= " + req.body.email + " WHERE id=" + req.body.id;
	executeQuery(res, query);
});
app.put("/api/editstudentprefs", function(req , res){
	sql.close();
	var query = "UPDATE [student] SET MajorID= " + req.body.MajorID + " , JobTypeID= " + req.body.JobTypeID + " , Status= " + req.body.Status + " , TransferID= " + req.body.TransferID + " WHERE StudentID=" + req.body.StudentID;
	executeQuery(res, query);
});

/* Advisor/Student: Create New Plan */
//Inserts a set of preferences into the table ParameterSet for plan generation
//NOTE: Table ParameterSet's columns do not currently reflect the updated parameter list. This query, however, does. It should match the table once its updated
app.post("/api/plans/prefdata" , function(req , res) {
	sql.close();
	var query = "INSERT INTO [parameterSet](MajorID, SchoolID, JobTypeID, TotalQuarterCredits, MajorQuarterCredits, EngCourseID, MathCourseID, Status, SummerPreference, EnrollmentType) VALUES ('" + req.body.MajorID + "', '" + req.body.SchoolID + "', '" + req.body.JobTypeID + "', '" + req.body.TotalQuarterCredits + "', '" + req.body.MajorQuarterCredits + "', '" + req.body.EngCourseID + "', '" + req.body.MathCourseID + "', '" + req.body.Status + "', '" + req.body.SummerOption + "', '" + req.body.EnrollmentType + "')";
	executeQuery(res, query);
});

/* Advisor/Student: Save New Plan */
//Saves a newly generated plan to the database
//NOTE: This is an outline of what's expected to be returned from the algorithm. As of this date, it is not implemented yet.
//Assumption: The new plan data should be capture by this API BEFORE the plan is saved by the user, as soon as its received by the front end
//Assumption: the API to post to StudyPlan is separate because it has to be called for each course entry received from the algo
app.post("/api/plans/savenew1" , function(req , res) {
	sql.close();
	var query = "INSERT INTO [GeneratedPlan, StudentStudyPlan](ParameterSetID, Status, StudentID, PlanID, Status) VALUES ('" + req.body.ParameterSetID + "', '" + req.body.Status + "', '" + req.body.StudentID + "', '" + req.body.Status + "')";
	executeQuery(res, query);
});
app.post("/api/plans/savenew2" , function(req, res) {
	sql.close();
	var query = "INSERT INTO [StudyPlan](PlanID, QuarterID, YearID, CourseID) VALUES ('" + req.body.PlanID + "', '" + req.body.QuarterID + "', '" + req.body.YearID + "', '" + req.body.CourseID + "')";
	executeQuery(res, query);
});

/* Advisor/Student: Display Active Plan */
//Assumption: plan is already saved by the user. For a new plan direct from the algorithm, use the Save New Plan API
//This API doesn't currently work because there is no Status field in the database indicated if a plan is active. This needs to be added to function. 
app.get("/api/plans/displayactive/:id/:pid", function(req , res){
	sql.close();
	var query = "SELECT QuarterID, YearID, CourseID FROM [StudyPlan] , [StudentStudyPlan] WHERE StudentStudyPlan.PlanID = StudyPlan.PlanID AND StudentStudyPlan.StudentID=" + req.params.id + " AND StudyPlan.PlanID=" + req.params.pid + " AND StudentStudyPlan.Status=1";
	executeQuery(res, query);
});

/* Advisor/Student: Display Inactive Plans  */
//IMPORTANT: This API is tentative and dependent on database changes. Please review the current database iteration and adjust accordingly
//This API doesn't currently work because there is no Status field in the database indicated if a plan is active. This needs to be added to function. 
app.get("/api/plans/displayinactive/:id/:pid", function(req , res){
	sql.close();
	var query = "SELECT QuarterID, YearID, CourseID FROM [StudyPlan] , [StudentStudyPlan] WHERE StudentStudyPlan.PlanID = StudyPlan.PlanID AND StudentStudyPlan.StudentID=" + req.params.id + " AND StudyPlan.PlanID=" + req.params.pid + " AND StudentStudyPlan.Status=0";
	executeQuery(res, query);
});

/* Advisor/Student: Save Plan */
//IMPORTANT: This API is tentative and dependent on database changes. Please review the current database iteration and adjust accordingly
//Assumptions: Because each course is saved in the database as its own row, this API has to be called for each course in the plan. 
app.patch("/api/plans/saveplan" , function(req , res) {
	sql.close();
	var query = "UPDATE [StudyPlan, StudentStudyPlan] SET QuarterID='" + req.body.QuarterID + "', YearID='" + req.body.YearID + "', CourseID='" + req.body.CourseID + "' WHERE PlanID=" + req.body.PlanID;
	executeQuery(res, query);
});

/* Advisory/Student: Save As Active Plan */ 
//Updates the current plan to become active and sets the previously active plan to inactive
app.patch("/api/plans/setactiveplan" , function(req , res) {
	sql.close();
	var query = "UPDATE [GeneratedPlan, StudentStudyPlan] SET GeneratedPlan.Status=1, StudentStudyPlan.Status=1 WHERE StudentStudyPlan.PlanID='" + req.body.PlanID + "' AND GeneratedPlan.ID= '" + req.body.PlanID;
	executeQuery(res, query);
});

/* Advisor: Update Plan Rating */
//Updates a plan's rating when an advisor rates a plan
//Based on PlanID, which links to GeneratedPlan
app.patch("/api/planratings/update", function(req , res){
	sql.close();
	var query = "UPDATE [PlanRating] SET DescriptionID=" + req.body.DescriptionID + ", Stars=" + req.body.Stars + " WHERE PlanID= " + req.body.PlanID;
	executeQuery(res, query);
});

/* Advisor: Get pre-defined Plan Rating Descriptions */
//An API for populating the plan rating with pre-defined reasons for rating the plan lower than 5 stars
app.get("/api/planratings/descriptions", function(req , res){
	sql.close();
	var query = "SELECT Description from [PlanRatingDescription]";
	executeQuery(res, query);
});

/* Advisor/Student: Get Pre-defined Preferences */
//These APIs are for populating certain student preferences with pre-defined selections. 
app.get("/api/preferences/colleges", function(req , res){
	sql.close();
	var query = "SELECT ID, Name from [School]";
	executeQuery(res, query);
});
app.get("/api/preferences/degrees", function(req , res){
	sql.close();
	var query = "SELECT id, Name from [Major]";
	executeQuery(res, query);
});
app.get("/api/preferences/english", function(req , res){
	sql.close();
	var query = "SELECT course_number FROM [COURSES] WHERE use_catalog = 1 AND course_number LIKE 'ENGL%'";
	executeQuery(res, query);
});	
app.get("/api/preferences/math", function(req , res){
	sql.close();
	var query = "SELECT course_number FROM [COURSES] WHERE use_catalog = 1 AND course_number LIKE 'MATH%'";
	executeQuery(res, query);
});
